<?php

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

global $db_x, $db_timeline;
$db_timeline = isset($db_timeline) ? $db_timeline : $db_x . 'timeline';