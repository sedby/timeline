<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=page.edit.delete.done
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');
if (COT_PAGES == true)
{
	require_once cot_langfile('timeline', 'plug');
	require_once cot_incfile('timeline', 'plug');
	global $db, $db_timeline, $rpage, $usr;
	$urlparams = empty($rpage['page_alias']) ? array('c' => $rpage['page_cat'], 'id' => $id) : array('c' => $rpage['page_cat'], 'al' => $rpage['page_alias']);
	$update_data = array(
		'timeline_owner_id' => $usr['id'],
		'timeline_date' =>  $rpage['page_date'],
		'timeline_type' => 'page',
		'timeline_action' => 'delete',
		'timeline_type_id' => $id,
		'timeline_item_id' => $id,
		'timeline_url' => cot_url('page', $urlparams, '', true),
		'timeline_title' => $rpage['page_title']
	);
	$db->insert($db_timeline, $update_data );
}