<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=global
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

require_once cot_langfile('timeline', 'plug');
require_once cot_incfile('timeline', 'plug');

/**
 * Generates timeline widget
 * @param  string  $tpl        Template code
 * @param  integer $items      Number of items to show. 0 - all items
 * @return string              Parsed HTML
 */

function timeline($tpl = 'timeline', $items = 5)
{
	global $db, $db_timeline, $L;
	$t = new XTemplate(cot_tplfile($tpl, 'plug'));
	$sql = $db->query("SELECT * FROM $db_timeline ORDER BY timeline_id DESC LIMIT $items");
	$jj = 1;
	while ($row = $sql->fetch())
	{
		$t->assign(array(
			'PAGE_ROW_NUM' => $jj,
			'PAGE_ROW_ODDEVEN' => cot_build_oddeven($jj),
			'PAGE_ROW_DATE_STAMP' => $row['timeline_date'],
			'PAGE_ROW_ACTION' => $L['tl-action-'.$row['timeline_action']],
			'PAGE_ROW_ACTION_RAW' => $row['timeline_action'],
			'PAGE_ROW_TYPE' => $L['tl-type-'.$row['timeline_type']],
			'PAGE_ROW_TYPE_RAW' => $row['timeline_type'],
			'PAGE_ROW_SHORTTITLE' => $row['timeline_title'],
			'PAGE_ROW_URL' => $row['timeline_url']
		));
		$t->assign(cot_generate_usertags($row['timeline_owner_id'], 'PAGE_ROW_OWNER_'));
		$t->parse("MAIN.PAGE_ROW");
		$jj++;
	}
	$t->parse('MAIN');
	return $t->text();
}