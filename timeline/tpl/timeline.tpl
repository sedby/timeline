<!-- BEGIN: MAIN -->
					<ul class="list-unstyled">
<!-- BEGIN: PAGE_ROW -->
						<li>
							<i class="fa fa-exclamation-circle"></i>
							{PHP.L.User}
<!-- IF {PAGE_ROW_OWNER_ID} -->
							{PAGE_ROW_OWNER_NAME}
<!-- ENDIF -->
							{PAGE_ROW_ACTION} {PAGE_ROW_TYPE}
<!-- IF {PAGE_ROW_ACTION_RAW} == delete -->
							&laquo;{PAGE_ROW_SHORTTITLE}&raquo;
<!-- ELSE -->
<!-- IF {PAGE_ROW_ACTION_RAW} != register -->
							&laquo;<a href="{PAGE_ROW_URL}">{PAGE_ROW_SHORTTITLE}</a>&raquo;
<!-- ENDIF -->
<!-- ENDIF -->
							<span>{PAGE_ROW_DATE_STAMP|cot_date('j F Y', $this)}</span>
						</li>
<!-- END: PAGE_ROW -->
					</ul>
<!-- END: MAIN -->