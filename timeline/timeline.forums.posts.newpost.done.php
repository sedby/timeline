<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=forums.posts.newpost.done
Tags=
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

if (COT_FORUMS == true)
{ 
	require_once cot_langfile('timeline', 'plug');
	require_once cot_incfile('timeline', 'plug');
	global $db, $db_timeline, $usr, $rmsg, $p;
	$tlpid = (int)$rmsg['fp_topicid'];
	$sql = $db->query("SELECT * FROM $db_forum_topics WHERE ft_id=$tlpid LIMIT 1")->fetch();
	$update_data = array(
		'timeline_owner_id' => $rmsg['fp_posterid'],
		'timeline_date' =>  $rmsg['fp_creation'], 
		'timeline_type' => 'post',
		'timeline_action' => 'create',
		'timeline_type_id' => $rmsg['fp_topicid'], 
		'timeline_item_id' => $p, 
		'timeline_url' => cot_url('forums', 'm=posts&id=' . $p , false, true),
		'timeline_title' => $sql['ft_title']
	);
	$db->insert($db_timeline, $update_data );
}