<?php

/**
 * English Language File for Timeline plugin
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

$L['info_desc'] = 'Site events logger';

$L['tl-action-create'] = 'added';
$L['tl-action-edit'] = 'edited';
$L['tl-action-delete'] = 'deleted';
$L['tl-action-register'] = 'registered user account';

$L['tl-type-page'] = 'page';
$L['tl-type-topic'] = 'topic';
$L['tl-type-post'] = 'post in topic';
$L['tl-type-comment'] = 'comment to page';