<?php

/**
 * Russian Language File for Timeline plugin
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

$L['info_desc'] = 'Лента событий на сайте';

$L['tl-action-create'] = 'добавил';
$L['tl-action-edit'] = 'отредактировал';
$L['tl-action-delete'] = 'удалил';
$L['tl-action-register'] = 'зарегистрировался на сайте';

$L['tl-type-page'] = 'страницу';
$L['tl-type-topic'] = 'топик';
$L['tl-type-post'] = 'пост в топик';
$L['tl-type-comment'] = 'комментарий к странице';