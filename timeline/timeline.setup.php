<?php
/* ====================
[BEGIN_COT_EXT]
Code=timeline
Name= Timeline
Category=administration-management
Description=Record events on website
Version=1.0
Date=2015-03-13
Author=Aliaksei Kobak
Copyright=&copy; seditio.by 2015
Notes=
Auth_guests=RW
Lock_guests=12345A
Auth_members=RW
Lock_members=
Requires_plugins=comments
Requires_modules=page,users
Recommends_plugins=
Recommends_modules=forums
[END_COT_EXT]
[BEGIN_COT_EXT_CONFIG]
[END_COT_EXT_CONFIG]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');