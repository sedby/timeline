<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=users.register.validate.done
Tags=
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');
if (COT_USERS == true)
{
	require_once cot_langfile('timeline', 'plug');
	require_once cot_incfile('timeline', 'plug');
	global $db, $db_timeline, $sys, $row;
	$tid = $row['user_id'];
	$sql = $db->query(" SELECT * FROM $db_users WHERE user_id=$tid LIMIT 1")->fetch();
	$update_data = array(
		'timeline_owner_id' => $row['user_id'],
		'timeline_date' =>  $sys['now'], 
		'timeline_type' => 'users',
		'timeline_action' => 'register',
		'timeline_type_id' => $row['user_id'], 
		'timeline_item_id' => $row['user_id'],
		'timeline_url' => cot_url('users', '&m=details&id=' . $row['user_id'], false, true),
		'timeline_title' => $sql['user_name']
	);
	$db->insert($db_timeline, $update_data);
}