CREATE TABLE IF NOT EXISTS `cot_timeline` (
	`timeline_id` INT(12) unsigned NOT NULL auto_increment,
	`timeline_owner_id` INT(12),
	`timeline_date` INT(12),
	`timeline_type` VARCHAR(256) NOT NULL default '',
	`timeline_action` VARCHAR(256) NOT NULL default '',
	`timeline_type_id` INT(12),
        `timeline_item_id` INT(12),
	`timeline_url` VARCHAR(256) NOT NULL default '',
	`timeline_title` VARCHAR(256) NOT NULL default '',
	`timeline_noshow` tinyint(1) unsigned NOT NULL default '0',
	PRIMARY KEY (`timeline_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
