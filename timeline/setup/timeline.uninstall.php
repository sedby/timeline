<?php

/**
 * Set all extended users rights to default state before delete plugin
 *
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

// Подключаем, если есть файл  в подкаталоге плагина с функциями вынесеными в отдельный файл functions.имяплагина.php
// require_once cot_incfile('timeline', 'plug');

global $db;

$db->query("DROP TABLE IF EXISTS `cot_timeline`");