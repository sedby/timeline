<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=comments.delete
Tags=
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');

require_once cot_langfile('timeline', 'plug');
require_once cot_incfile('timeline', 'plug');
global $db, $sys, $db_pages, $db_timeline, $comarray, $id, $item;
$sql = $db->query("SELECT * FROM $db_pages WHERE page_id=$item LIMIT 1")->fetch();
$update_data = array(
	'timeline_owner_id' => $comarray['com_authorid'],
	'timeline_date' => $sys['now'],
	'timeline_type' => 'comment',
	'timeline_action' => 'delete',
	'timeline_type_id' => (int)$item,	// parent id
	'timeline_item_id' => $id,			// comment id
	'timeline_url' => cot_url($url_area, $url_params, '#c' . $id, true, true),
	'timeline_title' => $sql['page_title']
);
$db->insert($db_timeline, $update_data );