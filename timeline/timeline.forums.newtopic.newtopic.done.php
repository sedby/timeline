<?php
/* ====================
[BEGIN_COT_EXT]
Hooks=forums.newtopic.newtopic.done
Tags=
[END_COT_EXT]
==================== */

/**
 * @package Timeline
 * @version 1.0
 * @author Aliaksei Kobak
 * @copyright (c) seditio.by 2015
 */

defined('COT_CODE') or die('Wrong URL');
if (COT_FORUMS == true)
{
	require_once cot_langfile('timeline', 'plug');
	require_once cot_incfile('timeline', 'plug');
	global $db, $db_timeline, $rmsg, $rtopic;
	$update_data = array(
		'timeline_owner_id' => $rmsg['fp_posterid'],
		'timeline_date' =>  $rmsg['fp_creation'],
		'timeline_type' => 'topic',
		'timeline_action' => 'create',
		'timeline_type_id' => $rmsg['fp_topicid'], 
		'timeline_item_id' => $rmsg['fp_topicid'],
		'timeline_url' => cot_url('forums', 'm=posts&q=' . $rmsg['fp_topicid'] , false, true),
		'timeline_title' => $rtopic['ft_title']
	);
	$db->insert($db_timeline, $update_data );
} 